#include "parameters.h"
#include "prototypes.h"

/* -------------------------- 1-Wire Master Addressing  ---------------------------*/

static uint8_t ucAddress1WM[NUM_TEMP_SENSORS] = { 0b0011011,   //I2C-1
                                                  0b0011000,   //I2C-1
                                                  0b0011001,   //I2C-1
                                                  0b0011010 }; //I2C-1

static int32_t ulTimerDS18B20[NUM_TEMP_SENSORS] = { TIMER2_BASE,
                                                    TIMER3_BASE,
                                                    TIMER4_BASE,
                                                    TIMER5_BASE };

volatile bool bTimeoutDS18B20[NUM_TEMP_SENSORS] = { false };

bool bWriteCommand1WM(uint8_t ucSensorNum, uint8_t ucByteNum, uint8_t* ucMsgWrite, eTempMode* eMode)
{
    if (!bWriteI2C(I2C1_BASE, ucMsgWrite, ucByteNum, ucAddress1WM[ucSensorNum]))
    {
        eMode[ucSensorNum] = ErrorI2C;
        return false;
    }
    else
    {
        //Start timeout timer
        TimerLoadSet(ulTimerDS18B20[ucSensorNum], TIMER_BOTH, SysCtlClockGet() * DS18B20_TIMEOUT);
        TimerEnable(ulTimerDS18B20[ucSensorNum], TIMER_BOTH);
    }

    return true;
}

bool bReadStatus1WM(uint8_t ucSensorNum, uint8_t* ucMsgReceive, eTempMode* eMode)
{
    //Read 1WM Status Register and check for I2C errors
    if (!bReceiveI2C(I2C1_BASE, ucMsgReceive, 1, ucAddress1WM[ucSensorNum]))
    {
        eMode[ucSensorNum] = ErrorI2C;
        return false;
    }

    //Check for SD errors
    if ((ucMsgReceive[0] & MASK_SD))
    {
        if (eMode[ucSensorNum] != ErrorSD) eMode[ucSensorNum] = ErrorSD; //short detected
        return false;
    }

    //Check for 1WM reset events
    if (ucMsgReceive[0] & MASK_RST)
    {
        if (eMode[ucSensorNum] != DS2482100_Configure)
        {
            eMode[ucSensorNum] = DS2482100_Configure; //1WM reset
            return false;
        }
    }

    //Check for DS18B20 timeout errors
    if (bTimeoutDS18B20[ucSensorNum])
    {
        if ((ucMsgReceive[0] & MASK_1WB) == NULL && (ucMsgReceive[0] & MASK_PPD) != NULL) //1WB = 0, PPD = 1
        {
            bTimeoutDS18B20[ucSensorNum] = false;
        }
        else
        {
            if (eMode[ucSensorNum] != Timeout) eMode[ucSensorNum] = Timeout;
            return false;
        }
    }
    else if ((ucMsgReceive[0] & MASK_1WB) == NULL && (ucMsgReceive[0] & MASK_PPD) != NULL) //1WB = 0, PPD = 1
    {
        //Presence pulse detected, 1-Wire ready, clear timeout
        bTimeoutDS18B20[ucSensorNum] = false;
        TimerDisable(ulTimerDS18B20[ucSensorNum], TIMER_BOTH);
    }

    //No errors
    return true;
}

bool bReadData1WM(uint8_t ucSensorNum, uint8_t* ucMsgReceive, eTempMode* eMode)
{
    //Read 1WM Status Register and check for I2C errors
    if (!bReceiveI2C(I2C1_BASE, ucMsgReceive, 1, ucAddress1WM[ucSensorNum]))
    {
        eMode[ucSensorNum] = ErrorI2C;
        return false;
    }

    //No errors - return mode setting
    return true;
}

void IntHandlerTimerDS18B20_1(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER2_BASE, true);
    TimerIntClear(TIMER2_BASE, ui32status);
    bTimeoutDS18B20[0] = true;
}

void IntHandlerTimerDS18B20_2(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER3_BASE, true);
    TimerIntClear(TIMER3_BASE, ui32status);
    bTimeoutDS18B20[1] = true;
}

void IntHandlerTimerDS18B20_3(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER4_BASE, true);
    TimerIntClear(TIMER4_BASE, ui32status);
    bTimeoutDS18B20[2] = true;
}

void IntHandlerTimerDS18B20_4(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER5_BASE, true);
    TimerIntClear(TIMER5_BASE, ui32status);
    bTimeoutDS18B20[3] = true;
}

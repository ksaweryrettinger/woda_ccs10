#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/include/mbutils.h>
#include "parameters.h"

/* -------------------------------- Callback Function - Read/Write Coil(s) --------------------------------------*/

#if (MBCB_COILS_DEFAULT)

eMBErrorCode eMBRegCoilsCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils, eMBRegisterMode eMode)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usRegIndex;

    usAddress -= 1;

    if ((usAddress >= COILS_START) && (usAddress + usNCoils <= COILS_START + COILS_NUM))
    {
        usRegIndex = (USHORT) (usAddress - COILS_START);

        switch (eMode)
        {
            case MB_REG_READ:
                while (usNCoils > 0)
                {
                    UCHAR ucResult = xMBUtilGetBits(ucMBCoils, usRegIndex, 1);
                    xMBUtilSetBits(pucRegBuffer, usRegIndex - (usAddress - COILS_START), 1, ucResult);
                    usRegIndex++;
                    usNCoils--;
                }
                break;

            case MB_REG_WRITE:
                while (usNCoils > 0)
                {
                    UCHAR ucResult = xMBUtilGetBits(pucRegBuffer, usRegIndex - (usAddress - COILS_START), 1);
                    xMBUtilSetBits(ucMBCoils, usRegIndex, 1, ucResult);
                    usRegIndex++;
                    usNCoils--;
                }
                break;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

#endif

/* ------------------------------- Callback Function - Read Discrete Input(s) ------------------------------------*/

#if (MBCB_DISCRETE_DEFAULT)

eMBErrorCode eMBRegDiscreteCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usRegIndex;

    usAddress -= 1;

    if ((usAddress >= DISCRETES_START) && (usAddress + usNDiscrete <= DISCRETES_START + DISCRETES_NUM))
    {
        usRegIndex = (USHORT) (usAddress - DISCRETES_START);

        while (usNDiscrete > 0)
        {
            UCHAR ucResult = xMBUtilGetBits(ucMBDiscretes, usRegIndex, 1);
            xMBUtilSetBits(pucRegBuffer, usRegIndex - (usAddress - DISCRETES_START), 1, ucResult);
            usRegIndex++;
            usNDiscrete--;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

#endif

/* -------------------------------- Callback Function - Read Input Register(s) -----------------------------------*/

#if (MBCB_INPUTREG_DEFAULT)

eMBErrorCode eMBRegInputCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usRegIndex;

    usAddress -= 1;

    if ((usAddress >= INPUT_REGISTERS_START) && (usAddress + usNRegs <= INPUT_REGISTERS_START + INPUT_REGISTERS_NUM))
    {
        usRegIndex = (USHORT) (usAddress - INPUT_REGISTERS_START);

        while (usNRegs > 0)
        {
            *pucRegBuffer++ = (UCHAR) (usMBInputReg[usRegIndex] >> 8);
            *pucRegBuffer++ = (UCHAR) (usMBInputReg[usRegIndex] & 0xFF);
            usRegIndex++;
            usNRegs--;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

#endif

/* --------------------------- Callback Function - Read/Write Holding Register(s) --------------------------------*/

#if (MBCB_HOLDING_DEFAULT)


eMBErrorCode eMBRegHoldingCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usRegIndex;

    usAddress -= 1;

    if ((usAddress >= HOLDING_REGISTERS_START) && (usAddress + usNRegs <= HOLDING_REGISTERS_START + HOLDING_REGISTERS_NUM))
    {
        usRegIndex = (USHORT) (usAddress - HOLDING_REGISTERS_START);
        switch (eMode)
        {
            /* Pass current register values to the protocol stack. */
            case MB_REG_READ:
                while (usNRegs > 0)
                {
                    *pucRegBuffer++ = (UCHAR)(usMBHoldingReg[usRegIndex] >> 8);
                    *pucRegBuffer++ = (UCHAR)(usMBHoldingReg[usRegIndex] & 0xFF);
                    usRegIndex++;
                    usNRegs--;
                }
                break;

            /* Update current register values with new values from the
             * protocol stack. */
            case MB_REG_WRITE:
                while (usNRegs > 0)
                {
                    usMBHoldingReg[usRegIndex] = *pucRegBuffer++ << 8;
                    usMBHoldingReg[usRegIndex] |= *pucRegBuffer++;
                    usRegIndex++;
                    usNRegs--;
                }
                break;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

#endif

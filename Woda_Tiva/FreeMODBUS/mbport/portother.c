#include <FreeMODBUS/mbport/include/port.h>
#include <parameters.h>

/* ----------------------- Other functions -----------------------------*/

void InitLED(void)
{
    if (!SysCtlPeripheralReady(MB_LED_PERIPHERAL)) SysCtlPeripheralEnable(MB_LED_PERIPHERAL);
    GPIOPinTypeGPIOOutput(MB_LED_PORT, MB_LED_PIN);
}

void LEDStatus(BOOL bStatus)
{
    if (bStatus) GPIOPinWrite(MB_LED_PORT, MB_LED_PIN, MB_LED_PIN); //switch ON LED
    else GPIOPinWrite(MB_LED_PORT, MB_LED_PIN, 0); //switch OFF LED
}

void ModbusInterruptDisable(void)
{
    IntDisable(MB_UART_INT);
    IntDisable(INT_TIMER0A);
}

void ModbusInterruptEnable(void)
{
    IntEnable(MB_UART_INT);
    IntEnable(INT_TIMER0A);
}

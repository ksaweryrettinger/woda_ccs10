/* 
 * FreeModbus Libary: A portable Modbus implementation for Modbus ASCII/RTU.
 * Copyright (c) 2006-2018 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* ----------------------- System includes ----------------------------------*/
#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/include/mbconfig.h>
#include <FreeMODBUS/include/mbdiag.h>
#include <FreeMODBUS/include/mbframe.h>
#include <FreeMODBUS/include/mbfunc.h>
#include <FreeMODBUS/include/mbport.h>
#include <FreeMODBUS/include/mbproto.h>
#include <FreeMODBUS/mbport/include/port.h>
#include <stdlib.h>
#include <string.h>

#if MB_RTU_ENABLED == 1
#include <FreeMODBUS/rtu/mbrtu.h>
#endif
#if MB_ASCII_ENABLED == 1
#include <FreeMODBUS/ascii/mbascii.h>
#endif
#if MB_TCP_ENABLED == 1
#include <FreeMODBUS/tcp/mbtcp.h>
#endif

#ifndef MB_PORT_HAS_CLOSE
#define MB_PORT_HAS_CLOSE 0
#endif

/* ----------------------- Static variables ---------------------------------*/

static UCHAR ucMBAddress;
static eMBMode eMBCurrentMode;

static enum
{
    STATE_ENABLED, STATE_DISABLED, STATE_NOT_INITIALIZED
} eMBState = STATE_NOT_INITIALIZED;

/* Functions pointer which are initialized in eMBInit( ). Depending on the
 * mode (RTU or ASCII) the are set to the correct implementations.
 */
static peMBFrameSend peMBFrameSendCur;
static pvMBFrameStart pvMBFrameStartCur;
static pvMBFrameStop pvMBFrameStopCur;
static peMBFrameReceive peMBFrameReceiveCur;
static pvMBFrameClose pvMBFrameCloseCur;

/* Callback functions required by the porting layer. They are called when
 * an external event has happend which includes a timeout or the reception
 * or transmission of a character.
 */
BOOL (*pxMBFrameCBByteReceived)(void);
BOOL (*pxMBFrameCBTransmitterEmpty)(void);
BOOL (*pxMBPortCBTimerExpired)(void);

BOOL (*pxMBFrameCBReceiveFSMCur)(void);
BOOL (*pxMBFrameCBTransmitFSMCur)(void);

/* An array of Modbus functions handlers which associates Modbus function
 * codes with implementing functions.
 */
static xMBFunctionHandler xFuncHandlers[MB_FUNC_HANDLERS_MAX] =
        {
#if MB_FUNC_OTHER_REP_SLAVEID_ENABLED > 0
        {   MB_FUNC_OTHER_REPORT_SLAVEID, eMBFuncReportSlaveID},
#endif
#if MB_FUNC_READ_INPUT_ENABLED > 0
        {   MB_FUNC_READ_INPUT_REGISTER, eMBFuncReadInputRegister},
#endif
#if MB_FUNC_READ_HOLDING_ENABLED > 0
        {   MB_FUNC_READ_HOLDING_REGISTER, eMBFuncReadHoldingRegister},
#endif
#if MB_FUNC_WRITE_MULTIPLE_HOLDING_ENABLED > 0
        {   MB_FUNC_WRITE_MULTIPLE_REGISTERS, eMBFuncWriteMultipleHoldingRegister},
#endif
#if MB_FUNC_WRITE_HOLDING_ENABLED > 0
        {   MB_FUNC_WRITE_REGISTER, eMBFuncWriteHoldingRegister},
#endif
#if MB_FUNC_READWRITE_HOLDING_ENABLED > 0
        {   MB_FUNC_READWRITE_MULTIPLE_REGISTERS, eMBFuncReadWriteMultipleHoldingRegister},
#endif
#if MB_FUNC_READ_COILS_ENABLED > 0
        {   MB_FUNC_READ_COILS, eMBFuncReadCoils},
#endif
#if MB_FUNC_WRITE_COIL_ENABLED > 0
        {   MB_FUNC_WRITE_SINGLE_COIL, eMBFuncWriteCoil},
#endif
#if MB_FUNC_WRITE_MULTIPLE_COILS_ENABLED > 0
        {   MB_FUNC_WRITE_MULTIPLE_COILS, eMBFuncWriteMultipleCoils},
#endif
#if MB_FUNC_READ_DISCRETE_INPUTS_ENABLED > 0
        {   MB_FUNC_READ_DISCRETE_INPUTS, eMBFuncReadDiscreteInputs},
#endif
#if MB_FUNC_DIAG_READ_EXCEPTION_ENABLED > 0
        {   MB_FUNC_DIAG_READ_EXCEPTION, eMBFuncDiagReadException},
#endif
#if MB_FUNC_DIAG_DIAGNOSTIC_ENABLED > 0
        {   MB_FUNC_DIAG_DIAGNOSTIC, eMBFuncDiagDiagnostics},
#endif
#if MB_FUNC_DIAG_GET_COM_EVENT_CNT_ENABLED > 0
        {   MB_FUNC_DIAG_GET_COM_EVENT_CNT, eMBFuncDiagGetCommEventCnt},
#endif
#if MB_FUNC_DIAG_GET_COM_EVENT_LOG_ENABLED > 0
        {   MB_FUNC_DIAG_GET_COM_EVENT_LOG, eMBFuncDiagGetCommEventLog},
#endif
    };

/* ----------------------- Start implementation -----------------------------*/
eMBErrorCode eMBInit(eMBMode eMode, UCHAR ucSlaveAddress, UCHAR ucPort,
                     ULONG ulBaudRate, eMBParity eParity)
{
    eMBErrorCode eStatus = MB_ENOERR;

    /* check preconditions */
    if ((ucSlaveAddress == MB_ADDRESS_BROADCAST)
            || (ucSlaveAddress < MB_ADDRESS_MIN)
            || (ucSlaveAddress > MB_ADDRESS_MAX))
    {
        eStatus = MB_EINVAL;
    }
    else
    {
        ucMBAddress = ucSlaveAddress;

        (void) InitLED(); //initialize LED GPIO output

        switch (eMode)
        {

        #if (MB_RTU_ENABLED > 0)

            case MB_RTU:
            pvMBFrameStartCur = eMBRTUStart;
            pvMBFrameStopCur = eMBRTUStop;
            peMBFrameSendCur = eMBRTUSend;
            peMBFrameReceiveCur = eMBRTUReceive;
            pvMBFrameCloseCur = MB_PORT_HAS_CLOSE ? vMBPortClose : NULL;
            pxMBFrameCBByteReceived = xMBRTUReceiveFSM;
            pxMBFrameCBTransmitterEmpty = xMBRTUTransmitFSM;
            pxMBPortCBTimerExpired = xMBRTUTimerT35Expired;

            eStatus = eMBRTUInit( ucMBAddress, ucPort, ulBaudRate, eParity );
            break;

        #endif

        #if (MB_ASCII_ENABLED > 0)

            case MB_ASCII:
            pvMBFrameStartCur = eMBASCIIStart;
            pvMBFrameStopCur = eMBASCIIStop;
            peMBFrameSendCur = eMBASCIISend;
            peMBFrameReceiveCur = eMBASCIIReceive;
            pvMBFrameCloseCur = MB_PORT_HAS_CLOSE ? vMBPortClose : NULL;
            pxMBFrameCBByteReceived = xMBASCIIReceiveFSM;
            pxMBFrameCBTransmitterEmpty = xMBASCIITransmitFSM;
            pxMBPortCBTimerExpired = xMBASCIITimerT1SExpired;

            eStatus = eMBASCIIInit( ucMBAddress, ucPort, ulBaudRate, eParity );
            break;

        #endif

        default:

            eStatus = MB_EINVAL;
        }

        if (eStatus == MB_ENOERR)
        {
            if (!xMBPortEventInit())
            {
                /* port dependent event module initialization failed. */
                eStatus = MB_EPORTERR;
            }
            else
            {

                #if (MB_FUNC_OTHER_REP_SLAVEID_ENABLED > 0)
                        eMBSetSlaveID(ucSlaveAddress, TRUE, NULL, 0);
                #endif

                eMBCurrentMode = eMode;
                eMBState = STATE_DISABLED;
            }
        }
    }
    return eStatus;
}

#if (MB_TCP_ENABLED > 0)

eMBErrorCode eMBTCPInit( USHORT ucTCPPort )
{
    eMBErrorCode eStatus = MB_ENOERR;

    if( ( eStatus = eMBTCPDoInit( ucTCPPort ) ) != MB_ENOERR )
    {
        eMBState = STATE_DISABLED;
    }
    else if( !xMBPortEventInit( ) )
    {
        /* Port dependent event module initalization failed. */
        eStatus = MB_EPORTERR;
    }
    else
    {
        pvMBFrameStartCur = eMBTCPStart;
        pvMBFrameStopCur = eMBTCPStop;
        peMBFrameReceiveCur = eMBTCPReceive;
        peMBFrameSendCur = eMBTCPSend;
        pvMBFrameCloseCur = MB_PORT_HAS_CLOSE ? vMBTCPPortClose : NULL;
        ucMBAddress = MB_TCP_PSEUDO_ADDRESS;
        eMBCurrentMode = MB_TCP;
        eMBState = STATE_DISABLED;
    }
    return eStatus;
}

#endif

eMBErrorCode eMBRegisterCB(UCHAR ucFunctionCode, pxMBFunctionHandler pxHandler)
{
    int i;
    eMBErrorCode eStatus;

    if ((0 < ucFunctionCode) && (ucFunctionCode <= 127))
    {
        ENTER_CRITICAL_SECTION( );
        if (pxHandler != NULL)
        {
            for (i = 0; i < MB_FUNC_HANDLERS_MAX; i++)
            {
                if ((xFuncHandlers[i].pxHandler == NULL)
                        || (xFuncHandlers[i].pxHandler == pxHandler))
                {
                    xFuncHandlers[i].ucFunctionCode = ucFunctionCode;
                    xFuncHandlers[i].pxHandler = pxHandler;
                    break;
                }
            }
            eStatus = (i != MB_FUNC_HANDLERS_MAX) ? MB_ENOERR : MB_ENORES;
        }
        else
        {
            for (i = 0; i < MB_FUNC_HANDLERS_MAX; i++)
            {
                if (xFuncHandlers[i].ucFunctionCode == ucFunctionCode)
                {
                    xFuncHandlers[i].ucFunctionCode = 0;
                    xFuncHandlers[i].pxHandler = NULL;
                    break;
                }
            }
            /* Remove can't fail. */
            eStatus = MB_ENOERR;
        }
        EXIT_CRITICAL_SECTION( );
    }
    else
    {
        eStatus = MB_EINVAL;
    }
    return eStatus;
}

eMBErrorCode eMBClose(void)
{
    eMBErrorCode eStatus = MB_ENOERR;

    if (eMBState == STATE_DISABLED)
    {
        if (pvMBFrameCloseCur != NULL)
        {
            pvMBFrameCloseCur();
        }
    }
    else
    {
        eStatus = MB_EILLSTATE;
    }
    return eStatus;
}

eMBErrorCode eMBEnable(void)
{
    eMBErrorCode eStatus = MB_ENOERR;

    if (eMBState == STATE_DISABLED)
    {
        /* Activate the protocol stack. */
        pvMBFrameStartCur();
        eMBState = STATE_ENABLED;
    }
    else
    {
        eStatus = MB_EILLSTATE;
    }
    return eStatus;
}

eMBErrorCode eMBDisable(void)
{
    eMBErrorCode eStatus;

    if (eMBState == STATE_ENABLED)
    {
        pvMBFrameStopCur();
        eMBState = STATE_DISABLED;
        eStatus = MB_ENOERR;
    }
    else if (eMBState == STATE_DISABLED)
    {
        eStatus = MB_ENOERR;
    }
    else
    {
        eStatus = MB_EILLSTATE;
    }
    return eStatus;
}

eMBErrorCode eMBPoll(void)
{
    static UCHAR *ucMBFrame;
    static UCHAR ucRcvAddress;
    static UCHAR ucFunctionCode;
    static USHORT usLength;
    static eMBException eException;

    int i;
    eMBErrorCode eStatus = MB_ENOERR;
    eMBEventType eEvent;

    /* Check if the protocol stack is ready. */
    if (eMBState != STATE_ENABLED)
    {
        return MB_EILLSTATE;
    }

    /* Check if there is a event available. If not return control to caller.
     * Otherwise we will handle the event. */
    if (xMBPortEventGet(&eEvent) == TRUE)
    {
        switch (eEvent)
        {
        case EV_READY:

            break;

        case EV_FRAME_RECEIVED:

            LEDStatus(TRUE); //switch ON LED

            eStatus = peMBFrameReceiveCur(&ucRcvAddress, &ucMBFrame, &usLength);
            if (eStatus == MB_ENOERR)
            {
                (void)xMBDiagUpdateDiagCounter(DIAG_COUNT_1); //message received

                /* Check if the frame is for us. If not ignore the frame. */
                if ((ucRcvAddress == ucMBAddress) || (ucRcvAddress == MB_ADDRESS_BROADCAST))
                {
                    (void)xMBDiagUpdateDiagCounter(DIAG_COUNT_4); //address is correct
                    if (ucRcvAddress == MB_ADDRESS_BROADCAST)
                    {
                        (void)xMBDiagUpdateDiagCounter(DIAG_COUNT_5); //broadcast message detected
                        (void)xMBDiagUpdateEventLog(DIAG_EVENT_RCV_NORMAL | DIAG_EVENT_RCV_BROADCAST); //log broadcast message
                    }
                    else
                    {
                        if (ucMBFrame[0] != MB_FUNC_DIAG_GET_COM_EVENT_LOG) //events not created for log requests
                        {
                            (void)xMBDiagUpdateEventLog(DIAG_EVENT_RCV_NORMAL); //message will be processed
                        }
                    }
                    (void)xMBPortEventPost(EV_EXECUTE);
                }
            }
            else
            {
                (void)xMBDiagUpdateDiagCounter(DIAG_COUNT_2); //message check failed
                (void)xMBDiagUpdateEventLog(DIAG_EVENT_SND_NORMAL); //no message sent, still log send event

                if (xMBDiagIsCharOverrun()) //character overrun error detected when receiving message
                {
                    (void)xMBDiagUpdateEventLog(DIAG_EVENT_RCV_NORMAL | DIAG_EVENT_RCV_COMM_ERR | DIAG_EVENT_RCV_OVERRUN); //log comms and overrun error
                    (void)xMBDiagSetCharOverrun(FALSE); //clear overrun flag
                }
                else
                {
                    (void)xMBDiagUpdateEventLog(DIAG_EVENT_RCV_NORMAL | DIAG_EVENT_RCV_COMM_ERR); //log comms errors
                }

                LEDStatus(FALSE); //switch off LED
            }
            break;

        case EV_EXECUTE:

            ucFunctionCode = ucMBFrame[MB_PDU_FUNC_OFF];
            eException = MB_EX_ILLEGAL_FUNCTION;
            for (i = 0; i < MB_FUNC_HANDLERS_MAX; i++)
            {
                /* No more function handlers registered. Abort. */
                if (xFuncHandlers[i].ucFunctionCode == 0)
                {
                    break;
                }
                else if (xFuncHandlers[i].ucFunctionCode == ucFunctionCode)
                {
                    eException = xFuncHandlers[i].pxHandler(ucMBFrame, &usLength);
                    break;
                }
            }

            /* If the request was not sent to the broadcast address we return a reply. */
            if (ucRcvAddress != MB_ADDRESS_BROADCAST)
            {
                if (eException != MB_EX_NONE)
                {
                    /* An exception occured. Build an error frame and update diagnostics. */
                    usLength = 0;
                    ucMBFrame[usLength++] = (UCHAR)(ucFunctionCode | MB_FUNC_ERROR);
                    ucMBFrame[usLength++] = eException;

                    (void)xMBDiagUpdateDiagCounter(DIAG_COUNT_3); //update exception counter

                    /* Update event log and counters */
                    switch (eException)
                    {
                    #if MB_EX_ILLEGAL_FUNCTION_ENABLED > 0
                        case MB_EX_ILLEGAL_FUNCTION:
                            (void)xMBDiagUpdateEventLog(DIAG_EVENT_SND_NORMAL | DIAG_EVENT_SND_EX_READ); //log read exception
                            break;
                    #endif

                    #if MB_EX_ILLEGAL_DATA_ADDRESS_ENABLED > 0
                        case MB_EX_ILLEGAL_DATA_ADDRESS:
                            (void)xMBDiagUpdateEventLog(DIAG_EVENT_SND_NORMAL | DIAG_EVENT_SND_EX_READ); //log read exception
                            break;
                    #endif

                    #if MB_EX_ILLEGAL_DATA_VALUE_ENABLED > 0
                        case MB_EX_ILLEGAL_DATA_VALUE:
                            (void)xMBDiagUpdateEventLog(DIAG_EVENT_SND_NORMAL | DIAG_EVENT_SND_EX_READ); //log read exception
                            break;
                    #endif

                    #if MB_EX_SLAVE_DEVICE_FAILURE_ENABLED > 0
                        case MB_EX_SLAVE_DEVICE_FAILURE:
                            (void)xMBDiagUpdateEventLog(DIAG_EVENT_SND_NORMAL | DIAG_EVENT_SND_EX_ABORT); //log abort exception
                            break;
                    #endif

                    #if MB_EX_ACKNOWLEDGE_ENABLED > 0
                        case MB_EX_ACKNOWLEDGE:
                            (void)xMBDiagUpdateEventLog(DIAG_EVENT_SND_NORMAL | DIAG_EVENT_SND_EX_BUSY); //log busy exception
                            break;
                    #endif

                    #if MB_EX_SLAVE_BUSY_ENABLED > 0
                        case MB_EX_SLAVE_BUSY:
                            (void)xMBDiagUpdateEventLog(DIAG_EVENT_SND_NORMAL | DIAG_EVENT_SND_EX_BUSY); //log busy exception
                            (void)xMBDiagUpdateDiagCounter(DIAG_COUNT_7); //update slave busy counter
                            break;
                    #endif

                    #if MB_EX_NEGATIVE_ACKNOWLEDGE_ENABLED > 0
                        case MB_EX_NEGATIVE_ACKNOWLEDGE:
                            (void)xMBDiagUpdateEventLog(DIAG_EVENT_SND_NORMAL | DIAG_EVENT_SND_EX_NAK); //log NAK exception
                            break;
                    #endif
                    }
                }

                else //no exception
                {
                    if ((ucMBFrame[0] != MB_FUNC_DIAG_GET_COM_EVENT_CNT) && (ucMBFrame[0] != MB_FUNC_DIAG_GET_COM_EVENT_LOG)
                            && !xMBDiagIsCommsRestart() && !xMBDiagIsClearCounters())
                    {
                        (void)xMBDiagUpdateEventCounter(); //update event counter
                    }
                    else if (xMBDiagIsClearCounters())
                    {
                        xMBDiagSetClearCounters(FALSE); //clear flag
                    }

                    if (!xMBDiagIsCommsRestart())
                    {
                        if (ucMBFrame[0] != MB_FUNC_DIAG_GET_COM_EVENT_LOG)  //events not created for log requests
                        {
                            (void)xMBDiagUpdateEventLog(DIAG_EVENT_SND_NORMAL); //log normal response event
                        }
                    }
                    else
                    {
                        (void)xMBDiagUpdateEventLog(DIAG_EVENT_COMMS_RESTART); //communications just reset
                        (void)xMBDiagSetCommsRestart(FALSE); //clear flag
                    }

                    if (ucMBFrame[0] == MB_FUNC_DIAG_GET_COM_EVENT_LOG)
                    {
                        /* If event log requested, run function again to get updated values */
                        usLength = 1;
                        for (i = 0; i < MB_FUNC_HANDLERS_MAX; i++)
                        {
                            if (xFuncHandlers[i].ucFunctionCode == MB_FUNC_DIAG_GET_COM_EVENT_LOG)
                            {
                                (void)xFuncHandlers[i].pxHandler(ucMBFrame, &usLength);
                                break;
                            }
                        }
                    }
                }

                if ((eMBCurrentMode == MB_ASCII) && MB_ASCII_TIMEOUT_WAIT_BEFORE_SEND_MS)
                {
                    vMBPortTimersDelay(MB_ASCII_TIMEOUT_WAIT_BEFORE_SEND_MS);
                }

                /* Message processing complete, send response to master */
                eStatus = peMBFrameSendCur(ucMBAddress, ucMBFrame, usLength);
            }
            break;

        case EV_FRAME_SENT:

            LEDStatus(FALSE); //switch off LED
            break;
        }
    }
    return MB_ENOERR;
}

// Woda - TM4C123GH6PM
// Ksawery Wieczorkowski-Rettinger <kwrettinger@gmail.com>

/*---------------------------------- Includes ------------------------------------------*/

#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/mbport/include/port.h>
#include <FreeMODBUS/include/mbutils.h>
#include "prototypes.h"
#include "parameters.h"

/*----------------------------- Expander Addressing -------------------------------*/

static uint8_t ucAddressExpander[NUM_EXPANDERS] = { 0b0100000,   //I2C-1 (Expander U6)
                                                    0b0100000,   //I2C-0 (Expander U7)
                                                    0b0100001,   //I2C-0 (Expander U8)
                                                    0b0100100 }; //I2C-0 (Expander U9)

static uint8_t ucAddressADC[NUM_ADC] = { 0b1001110,   //I2C-0 (ADC #1, Inner Tank)
                                         0b1001010 }; //I2C-1 (ADC #2, Outer Tank)

/*----------------------------- Water Leak Detection ------------------------------*/

float usWaterLeakSmall = WATER_LEAK_SMALL_DEF;
float usWaterLeakLarge = WATER_LEAK_LARGE_DEF;

/*----------------------------- ADC Stopwatches ------------------------------*/

volatile uint8_t ucStopwatchADS1110[2] = { 0 };
volatile bool bStopwatchADC1[5] = { false };
volatile bool bStopwatchADC2[5] = { false };

int main(void)
{
    /*-------------------------- FPU Module -----------------------------------------------*/

    FPULazyStackingEnable();
    FPUEnable();

    /*------------------------ Local Variables ------------------------------------------*/

    //Temperature readings
    eTempMode eMode[NUM_TEMP_SENSORS] = { DS2482100_Reset };
    uint8_t ucTempReading[2] = { 0 };
    uint8_t ucMsgWrite[NUM_TEMP_SENSORS][I2C_MSG_NUMBYTES] = { 0 };
    uint8_t ucMsgReceive[I2C_MSG_NUMBYTES + 1] = { 0 };
    float Temperature[NUM_TEMP_SENSORS] = { 0 };

    //Expanders
    uint8_t ucExpanderOutputs[2] = { 0 };

    //Water Levels (ADC)
    uint16_t usADCReadingTemp = 0;
    uint16_t usADCReadingNum[NUM_ADC] = { 0 };
    uint32_t ulADCReadingSum[NUM_ADC] = { 0 };
    uint16_t usADCReadingNum4[NUM_ADC] = { 0 };
    uint32_t ulADCReadingSum4[NUM_ADC] = { 0 };
    uint16_t usADCReadingNum16[NUM_ADC] = { 0 };
    uint32_t ulADCReadingSum16[NUM_ADC] = { 0 };
    uint16_t usADCReadingNum32[NUM_ADC] = { 0 };
    uint32_t ulADCReadingSum32[NUM_ADC] = { 0 };
    uint16_t usADCReadingNum64[NUM_ADC] = { 0 };
    uint32_t ulADCReadingSum64[NUM_ADC] = { 0 };
    uint16_t usWaterInnerPrevious = 0;
    uint8_t ucWaterInnerLeakCount = 0;
    bool bWaterHistoryReady = false;

    //Water Low Signalling
    bool bWaterOuterLow = false;
    bool bWaterOuterHigh = false;
    bool bWaterInnerLow = false;
    bool bWaterInnerLeakSmall = false;
    bool bWaterInnerLeakLarge = false;

    //Counters
    uint8_t i = 0;
    uint8_t j = 0;

    //Unreferened variables
    UNREFERENCED(Temperature);

    /*-------------------------------- System Clock Configuration (50MHz) ----------------------------------*/

    SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    /*------------------------------------------ GPIO Peripherals ------------------------------------------*/

    //Enable GPIO peripherals
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA); //I2C
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB); //I2C, GPIO (Modbus LED)
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC); //UART (Modbus)
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD); //GPIO

    /*-------------------------------------------- GPIO Outputs --------------------------------------------*/

    GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

    /*----------------------------------------- GPIO Initialization ----------------------------------------*/

    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, 0);

    /*---------------------------------------------- I2C Pins ----------------------------------------------*/

    //I2C-0 (Expanders U7, U8, U9, Inner Tank ADS1110)
    GPIOPinConfigure(GPIO_PB2_I2C0SCL);
    GPIOPinConfigure(GPIO_PB3_I2C0SDA);
    GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
    GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);

    //I2C-1 (4x DS18B20 Sensors, Expander U6, Outer Tank ADS1110)
    GPIOPinConfigure(GPIO_PA6_I2C1SCL);
    GPIOPinConfigure(GPIO_PA7_I2C1SDA);
    GPIOPinTypeI2CSCL(GPIO_PORTA_BASE, GPIO_PIN_6);
    GPIOPinTypeI2C(GPIO_PORTA_BASE, GPIO_PIN_7);

    /*------------------------------------------ I2C Configuration -----------------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C1);
    I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), false); //standard speed
    I2CMasterInitExpClk(I2C1_BASE, SysCtlClockGet(), false); //standard speed

    /*------------------------------------------ I2C Timeout Timer -----------------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER1A);
    TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);

    /*--------------------------------------- DS18B20 Timeout Timers ---------------------------------------*/

    //DS18B20 #1
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);
    TimerConfigure(TIMER2_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER2A);
    TimerIntEnable(TIMER2_BASE, TIMER_TIMA_TIMEOUT);

    //DS18B20 #2
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER3);
    TimerConfigure(TIMER3_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER3A);
    TimerIntEnable(TIMER3_BASE, TIMER_TIMA_TIMEOUT);

    //DS18B20 #3
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER4);
    TimerConfigure(TIMER4_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER4A);
    TimerIntEnable(TIMER4_BASE, TIMER_TIMA_TIMEOUT);

    //DS18B20 #4
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER5);
    TimerConfigure(TIMER5_BASE, TIMER_CFG_ONE_SHOT);
    IntEnable(INT_TIMER5A);
    TimerIntEnable(TIMER5_BASE, TIMER_TIMA_TIMEOUT);

    /*-------------------------------------- ADS1110 Stopwatch Timers --------------------------------------*/

    //Timer 1
    SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER0);
    TimerConfigure(WTIMER0_BASE, TIMER_CFG_A_PERIODIC);
    IntEnable(INT_WTIMER0A);
    TimerIntEnable(WTIMER0_BASE, TIMER_TIMA_TIMEOUT);

    //Timer 2
    SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER1);
    TimerConfigure(WTIMER1_BASE, TIMER_CFG_A_PERIODIC);
    IntEnable(INT_WTIMER1A);
    TimerIntEnable(WTIMER1_BASE, TIMER_TIMA_TIMEOUT);

    //Start stopwatch timers
    TimerLoadSet64(WTIMER0_BASE, SysCtlClockGet());
    TimerLoadSet64(WTIMER1_BASE, SysCtlClockGet());
    TimerEnable(WTIMER0_BASE, TIMER_A);
    TimerEnable(WTIMER1_BASE, TIMER_A);

    /*--------------------------------- MODBUS Protocol Stack Initialization -------------------------------*/

    //Note: Modbus GPIO and UART configured in parameters.h
    (void) eMBInit(MB_MODE, MB_SLAVEID, MB_PORT, MB_BAUDRATE, MB_PARITY);
    (void) eMBEnable();

    while (1)
    {
        /*----------------------------------------------- Modbus -----------------------------------------------*/

        (void) eMBPoll();

        /*----------------------------------------- Temperature Sensors ----------------------------------------*/

        for (i = 0; i < NUM_TEMP_SENSORS; i++)
        {
            switch (eMode[i])
            {
                case DS2482100_Reset: //Send Reset Command to DS2482-100

                    //Reset 1WM
                    ucMsgWrite[i][0] = DRST;
                    ucMsgWrite[i][1] = NULL;
                    if (!bWriteCommand1WM(i, 1, ucMsgWrite[i], eMode)) break;

                    eMode[i] = DS2482100_Configure;
                    break;

                case DS2482100_Configure: //Setup DS2482-100 Configuration Register

                    //Read 1WM Status Register
                    if (!bReadStatus1WM(i, ucMsgReceive, eMode)) break;

                    //Wait for 1WB = 0 and RST = 1
                    if (((ucMsgReceive[0] & MASK_1WB) == NULL) && ((ucMsgReceive[0] & MASK_RST) != NULL))
                    {
                        //Set 1WM configuration register
                        ucMsgWrite[i][0] = WCFG;
                        ucMsgWrite[i][1] = WCFG_CMD;
                        if (!bWriteCommand1WM(i, 2, ucMsgWrite[i], eMode)) break;

                        //Set RP at status register
                        ucMsgWrite[i][0] = SETRP;
                        ucMsgWrite[i][1] = SETRP_SR;
                        if (!bWriteCommand1WM(i, 2, ucMsgWrite[i], eMode)) break;

                        //Switch mode to sensor initialization
                        eMode[i] = DS18B20_StartConfigure;
                        break;
                    }

                    break;

                case DS18B20_StartConfigure: //Start DS18B20 Configuration

                    //Read 1WM Status Register
                    if (!bReadStatus1WM(i, ucMsgReceive, eMode)) break;

                    //Wait for 1WB = 0
                    if (((ucMsgReceive[0] & MASK_1WB) == NULL))
                    {
                        //Initiate 1-Wire Reset
                        ucMsgWrite[i][0] = WRST;
                        ucMsgWrite[i][1] = NULL;
                        if (!bWriteCommand1WM(i, 1, ucMsgWrite[i], eMode)) break;

                        //Switch to waiting mode
                        eMode[i] = DS18B20_Configure;
                    }

                    break;

                case DS18B20_Configure: //Complete DS18B20 Configuration

                    //Read 1WM Status Register
                    if (!bReadStatus1WM(i, ucMsgReceive, eMode)) break;

                    //Wait for 1WB = 0 and PPD = 1
                    if (((ucMsgReceive[0] & MASK_1WB) == NULL) && ((ucMsgReceive[0] & MASK_PPD) != NULL))
                    {
                        //Contruct next message
                        if ((ucMsgReceive[0] & MASK_PPD) && ucMsgWrite[i][1] == NULL)
                        {
                            //DS18B20 Skip-Rom
                            ucMsgWrite[i][0] = WRBYTE;
                            ucMsgWrite[i][1] = SKPROM;
                        }
                        else if (ucMsgWrite[i][1] == SKPROM)
                        {
                            //DS18B20 Write to Scratchpad
                            ucMsgWrite[i][0] = WRBYTE;
                            ucMsgWrite[i][1] = WRSCR;
                        }
                        else if (ucMsgWrite[i][1] == WRSCR)
                        {
                            //DS18B20 TL Register
                            ucMsgWrite[i][0] = WRBYTE;
                            ucMsgWrite[i][1] = REG_TL; //TL = -10C
                        }
                        else if (ucMsgWrite[i][1] == REG_TL)
                        {
                            //DS18B20 TH Register
                            ucMsgWrite[i][0] = WRBYTE;
                            ucMsgWrite[i][1] = REG_TH; //TH = 100C
                        }
                        else if (ucMsgWrite[i][1] == REG_TH)
                        {
                            //DS18B20 Configuration Register
                            ucMsgWrite[i][0] = WRBYTE;
                            ucMsgWrite[i][1] = REG_CFG; //11-bit, 375ms conversion time
                        }
                        else if (ucMsgWrite[i][1] == REG_CFG)
                        {
                            //Clear write register
                            ucMsgWrite[i][0] = NULL;
                            ucMsgWrite[i][1] = NULL;

                            //Sensor initialization complete, switch to 'Reset' mode
                            eMode[i] = DS18B20_Reset;

                            break;
                        }

                        //Send command to 1WM
                        if (!bWriteCommand1WM(i, 2, ucMsgWrite[i], eMode)) break;
                    }

                    break;

                case DS18B20_Reset: //Reset DS18B20

                    //Set RP at status register
                    ucMsgWrite[i][0] = SETRP;
                    ucMsgWrite[i][1] = SETRP_SR;
                    if (!bWriteCommand1WM(i, 2, ucMsgWrite[i], eMode)) break;

                    //Read 1WM Status Register
                    if (!bReadStatus1WM(i, ucMsgReceive, eMode)) break;

                    //Wait for 1WB = 0
                    if (((ucMsgReceive[0] & MASK_1WB) == NULL))
                    {
                        //Initiate 1-Wire Reset
                        ucMsgWrite[i][0] = WRST;
                        ucMsgWrite[i][1] = NULL;
                        if (!bWriteCommand1WM(i, 1, ucMsgWrite[i], eMode)) break;

                        //Switch to waiting mode
                        eMode[i] = DS18B20_StartConversion;
                    }

                    break;

                case DS18B20_StartConversion: //Start DS18B20 Temperature Conversion

                    //Read 1WM Status Register
                    if (!bReadStatus1WM(i, ucMsgReceive, eMode)) break;

                    //Wait for 1WB = 0 and PPD = 1
                    if (((ucMsgReceive[0] & MASK_1WB) == NULL) && ((ucMsgReceive[0] & MASK_PPD) != NULL))
                    {
                        if (ucMsgWrite[i][1] == NULL)
                        {
                            //DS18B20 Skip-Rom
                            ucMsgWrite[i][0] = WRBYTE;
                            ucMsgWrite[i][1] = SKPROM;
                            bWriteCommand1WM(i, 2, ucMsgWrite[i], eMode);
                            break;
                        }
                        else if (ucMsgWrite[i][1] == SKPROM)
                        {
                            //DS18B20 Convert T
                            ucMsgWrite[i][0] = WRBYTE;
                            ucMsgWrite[i][1] = CONVT;
                            if (!bWriteCommand1WM(i, 2, ucMsgWrite[i], eMode)) break;

                            //Switch to waiting for temperature conversion (slave busy)
                            eMode[i] = DS18B20_SensorBusy;
                        }
                    }
                    else break;

                case DS18B20_SensorBusy: //Wait for DS18B20 Temperature Conversion

                    //Read 1WM Status Register
                    if (!bReadStatus1WM(i, ucMsgReceive, eMode)) break;

                    //Check for data ready
                    if (((ucMsgReceive[0] & MASK_1WB) == NULL) && (ucMsgWrite[i][1] != RDTS))  //Send DS18B20 Query
                    {
                        //Issue Read Time Slot
                        ucMsgWrite[i][0] = WBIT;
                        ucMsgWrite[i][1] = RDTS;
                        bWriteCommand1WM(i, 2, ucMsgWrite[i], eMode);
                        break;
                    }
                    else if (((ucMsgReceive[0] & MASK_1WB) == NULL) && (ucMsgWrite[i][1] == RDTS)) //DS18B20 Query Sent
                    {
                        //Clear write register
                        ucMsgWrite[i][0] = NULL;
                        ucMsgWrite[i][1] = NULL;

                        //Wait for SBR = 1
                        if ((ucMsgReceive[0] & MASK_SBR) != NULL)
                        {
                            //Initiate 1-Wire Reset
                            ucMsgWrite[i][0] = WRST;
                            ucMsgWrite[i][1] = NULL;
                            if (!bWriteCommand1WM(i, 1, ucMsgWrite[i], eMode)) break;

                            //Read DS18B20 Scratchpad
                            eMode[i] = DS18B20_ConversionReady;
                        }
                    }
                    else break;

                case DS18B20_ConversionReady: //DS18B20 Temperature Conversion Ready, Read Scratchpad

                    //Read 1WM Status Register
                    if (!bReadStatus1WM(i, ucMsgReceive, eMode)) break;

                    //Wait for 1WB = 0
                    if (((ucMsgReceive[0] & MASK_1WB) == NULL))
                    {
                        if (ucMsgWrite[i][1] == NULL)
                        {
                            //DS18B20 Skip-Rom
                            ucMsgWrite[i][0] = WRBYTE;
                            ucMsgWrite[i][1] = SKPROM;
                            bWriteCommand1WM(i, 2, ucMsgWrite[i], eMode);
                            break;
                        }
                        else if (ucMsgWrite[i][1] == SKPROM)
                        {
                            //DS18B20 Read Scratchpad
                            ucMsgWrite[i][0] = WRBYTE;
                            ucMsgWrite[i][1] = RDSCR;
                            if (!bWriteCommand1WM(i, 2, ucMsgWrite[i], eMode)) break;

                            //Switch to waiting for temperature conversion (slave busy)
                            eMode[i] = DS18B20_ReadTemperature;
                        }
                    }
                    else break;

                case DS18B20_ReadTemperature: //Read DS18B20 Temperature

                    //Read 1WM Status Register
                    if (!bReadStatus1WM(i, ucMsgReceive, eMode)) break;

                    //Wait for 1WB = 0
                    if (((ucMsgReceive[0] & MASK_1WB) == NULL))
                    {
                        for (j = 0; j < NUM_TEMP_BYTES; j++)
                        {
                            //Read Single Byte
                            ucMsgWrite[i][0] = RDBYTE;
                            ucMsgWrite[i][1] = NULL;
                            if (!bWriteCommand1WM(i, 1, ucMsgWrite[i], eMode)) break;

                            //Wait for 1WB = 0
                            while (1)
                            {
                                //Read 1WM Status Register
                                if (!bReadStatus1WM(i, ucMsgReceive, eMode)) break;
                                if (((ucMsgReceive[0] & MASK_1WB) == NULL)) break;
                            }

                            //Set RP at Read Data Register
                            ucMsgWrite[i][0] = SETRP;
                            ucMsgWrite[i][1] = SETRP_RD;
                            if (!bWriteCommand1WM(i, 2, ucMsgWrite[i], eMode)) break;

                            /*-------------------------------- Read Temperature Data ------------------------------*/

                            if (!bReadData1WM(i, &ucTempReading[j], eMode)) break;

                            /*-------------------------------------------------------------------------------------*/

                            //Set RP at status register
                            ucMsgWrite[i][0] = SETRP;
                            ucMsgWrite[i][1] = SETRP_SR;
                            if (!bWriteCommand1WM(i, 2, ucMsgWrite[i], eMode)) break;

                            //Wait for 1WB = 0
                            while (1)
                            {
                                //Read 1WM Status Register
                                if (!bReadStatus1WM(i, ucMsgReceive, eMode)) break;
                                if (((ucMsgReceive[0] & MASK_1WB) == NULL)) break;
                            }

                            //Check for mode changes and errors
                            if (eMode[i] != DS18B20_ReadTemperature) break;

                            //Reading temperature complete
                            if (j == (NUM_TEMP_BYTES - 1)) eMode[i] = DS18B20_Reset;
                        }
                    }

                    //Store new temperature reading
                    if (eMode[i] == DS18B20_Reset)
                    {
                        usMBInputReg[i] = ucTempReading[0];
                        usMBInputReg[i] |= ucTempReading[1] << 8;

                        //Convert to degrees C (debug only)
                        if (usMBInputReg[i] & 0x8000)
                        {
                            //Negative temperature - get 2's complement
                            usMBInputReg[i] = (usMBInputReg[i] ^ 0xFFFF) + 1;

                            Temperature[i] = (-1) * ((usMBInputReg[i] >> 4) + (((usMBInputReg[i] & 0x8) >> 3) * 0.5f) + (((usMBInputReg[i] & 0x4) >> 2) * 0.25f) + (((usMBInputReg[i] & 0x2) >> 1) * 0.125f));
                        }
                        else
                        {
                            Temperature[i] = ((usMBInputReg[i] >> 4) + (((usMBInputReg[i] & 0x8) >> 3) * 0.5f) + (((usMBInputReg[i] & 0x4) >> 2) * 0.25f) + (((usMBInputReg[i] & 0x2) >> 1) * 0.125f));
                        }
                    }

                    break;

                case ErrorSD: //DS2482-100 Short Detected

                    //Clear write register
                    ucMsgWrite[i][0] = NULL;
                    ucMsgWrite[i][1] = NULL;

                    //Update Modbus data
                    usMBInputReg[INPUTREG_ERROR_SD - 1] |= (1 << i);

                    //Read 1WM Status Register
                    if (!bReadStatus1WM(i, ucMsgReceive, eMode)) break;

                    //SD = 0, clear errors and switch to reset mode
                    if ((ucMsgReceive[0] & MASK_SD) == NULL)
                    {
                        usMBInputReg[INPUTREG_ERROR_SD - 1] &= ~(1 << i);
                        eMode[i] = DS18B20_Reset;
                    }

                    break;

                case ErrorI2C: //I2C Communication Errors

                    //Clear write register
                    ucMsgWrite[i][0] = NULL;
                    ucMsgWrite[i][1] = NULL;

                    //Update Modbus data
                    usMBInputReg[INPUTREG_I2C_ERRORS - 1] |= (1 << i);

                    //Attempt Reading 1WM Status Register
                    if (!bReadStatus1WM(i, ucMsgReceive, eMode)) break;

                    //Read successful, clear errors and switch to Reset mode
                    usMBInputReg[INPUTREG_I2C_ERRORS - 1] &= ~(1 << i);
                    eMode[i] = DS18B20_Reset;

                    break;

                case Timeout:

                    //Update local and Modbus data
                    Temperature[i] = 0;
                    usMBInputReg[i] = 0;
                    usMBInputReg[INPUTREG_ERROR_TIME - 1] |= (1 << i);

                    (void) bReadStatus1WM(i, ucMsgReceive, eMode);

                    //Attempt 1-Wire Reset
                    if ((ucMsgReceive[0] & MASK_1WB) == NULL)
                    {
                        ucMsgWrite[i][0] = WRST;
                        ucMsgWrite[i][1] = NULL;
                        if (!bWriteCommand1WM(i, 1, ucMsgWrite[i], eMode)) break;
                    }

                    //Read 1WM Status Register
                    if (!bReadStatus1WM(i, ucMsgReceive, eMode)) break;

                    //1WB = 0, clear errors and switch to reset mode
                    usMBInputReg[INPUTREG_ERROR_TIME - 1] &= ~(1 << i);
                    eMode[i] = DS18B20_Reset;
            }
        }

        /*----------------------------------- Outer Tank Water Level (ADC #2) ----------------------------------*/

        if (bReceiveI2C(I2C1_BASE, ucMsgReceive, 3, ucAddressADC[1]))
        {
            if (!(ucMsgReceive[2] & 0x80)) //check for ST/DRDY = 0
            {
                if ((ucMsgReceive[0] & 0x80) != 0) //negative ADC value
                {
                    usMBInputReg[INPUTREG_WATER_OUTER - 1] = 0;
                    usMBInputReg[INPUTREG_WATER_OUTER_4 - 1] = 0;
                    usMBInputReg[INPUTREG_WATER_OUTER_16 - 1] = 0;
                    usMBInputReg[INPUTREG_WATER_OUTER_32 - 1] = 0;
                    usMBInputReg[INPUTREG_WATER_OUTER_64 - 1] = 0;

                    //Clear counters and summation
                    usADCReadingNum[0] = 0;
                    ulADCReadingSum[0] = 0;
                }
                else //ADC value OK
                {
                    //Get ADC reading
                    usADCReadingTemp = (uint16_t) (ucMsgReceive[0] << 8);
                    usADCReadingTemp |= ucMsgReceive[1];

                    //Update counters and sum readings
                    usADCReadingNum[0]++;
                    ulADCReadingSum[0] += usADCReadingTemp;
                    usADCReadingNum4[0]++;
                    ulADCReadingSum4[0] += usADCReadingTemp;
                    usADCReadingNum16[0]++;
                    ulADCReadingSum16[0] += usADCReadingTemp;
                    usADCReadingNum32[0]++;
                    ulADCReadingSum32[0] += usADCReadingTemp;
                    usADCReadingNum64[0]++;
                    ulADCReadingSum64[0] += usADCReadingTemp;

                    //Calculate and store averages
                    if (bStopwatchADC1[0])
                    {
                        usMBInputReg[INPUTREG_WATER_OUTER - 1] = (uint16_t) (ulADCReadingSum[0] /  usADCReadingNum[0]);
                        usADCReadingNum[0] = 0;
                        ulADCReadingSum[0] = 0;
                        bStopwatchADC1[0] = false;
                    }
                    if (bStopwatchADC1[1]) //4s moving average
                    {
                        usMBInputReg[INPUTREG_WATER_OUTER_4 - 1] = (uint16_t) (ulADCReadingSum4[0] /  usADCReadingNum4[0]);
                        usADCReadingNum4[0] = 0;
                        ulADCReadingSum4[0] = 0;
                        bStopwatchADC1[1] = false;

                        //Check water level
                        bWaterOuterLow = false;
                        bWaterOuterHigh = false;

                        if (usMBInputReg[INPUTREG_WATER_OUTER_4 - 1] <= (((float) (WATER_OUTER_LOW * WATER_MAX_VOLUME)) / 100.0f)) bWaterOuterLow = true;
                        else if (usMBInputReg[INPUTREG_WATER_OUTER_4 - 1] >= (((float) (WATER_OUTER_HIGH * WATER_MAX_VOLUME)) / 100.0f)) bWaterOuterHigh = true;

                        //Signal low water level via Modbus
                        if (bWaterOuterLow) usMBInputReg[INPUTREG_WATER_LEVEL - 1] |= 0x0001;
                        else usMBInputReg[INPUTREG_WATER_LEVEL - 1] &= ~(0x0001);

                        //Signal high water level via Modbus
                        if (bWaterOuterHigh) usMBInputReg[INPUTREG_WATER_LEVEL - 1] |= 0x0002;
                        else usMBInputReg[INPUTREG_WATER_LEVEL - 1] &= ~(0x0002);
                    }
                    else if (bStopwatchADC1[2]) //16s moving average
                    {
                        usMBInputReg[INPUTREG_WATER_OUTER_16 - 1] = (uint16_t) (ulADCReadingSum16[0] /  usADCReadingNum16[0]);
                        usADCReadingNum16[0] = 0;
                        ulADCReadingSum16[0] = 0;
                        bStopwatchADC1[2] = false;
                    }
                    else if (bStopwatchADC1[3]) //32s moving average
                    {
                        usMBInputReg[INPUTREG_WATER_OUTER_32 - 1] = (uint16_t) (ulADCReadingSum32[0] /  usADCReadingNum32[0]);
                        usADCReadingNum32[0] = 0;
                        ulADCReadingSum32[0] = 0;
                        bStopwatchADC1[3] = false;
                    }
                    else if (bStopwatchADC1[4]) //64s moving average
                    {
                        usMBInputReg[INPUTREG_WATER_OUTER_64 - 1] = (uint16_t) (ulADCReadingSum64[0] /  usADCReadingNum64[0]);
                        usADCReadingNum64[0] = 0;
                        ulADCReadingSum64[0] = 0;
                        bStopwatchADC1[4] = false;

                        //Reset stopwatch timer and counter
                        TimerLoadSet(WTIMER0_BASE, TIMER_A, SysCtlClockGet());
                        TimerEnable(WTIMER0_BASE, TIMER_A);
                    }
                }
            }

            //Clear I2C error
            usMBInputReg[INPUTREG_I2C_ERRORS - 1] &= ~(MASK_I2C_ERROR_ADC2);
        }
        else usMBInputReg[INPUTREG_I2C_ERRORS - 1] |= MASK_I2C_ERROR_ADC2; //store I2C error

        /*---------------------------- Opto-Isolator Inputs (Pump Status) (Expander U6)-------------------------*/

        if (bReceiveI2C(I2C1_BASE, ucMsgReceive, 1, ucAddressExpander[EXPANDER_PUMPS]))
        {
            //Store 6x Pump Statuses
            usMBInputReg[INPUTREG_PUMPS - 1] = (uint16_t) ~(ucMsgReceive[0]);

            //Clear I2C error
            usMBInputReg[INPUTREG_I2C_ERRORS - 1] &= ~(MASK_I2C_ERROR_U6);
        }
        else usMBInputReg[INPUTREG_I2C_ERRORS - 1] |= MASK_I2C_ERROR_U6; //store I2C error

        /*----------------------------------- Inner Tank Water Level (ADC #1) ----------------------------------*/

        //Clear water leak flags
        if (ucMBCoils[0] & 0x02)
        {
            //Clear coil
            ucMBCoils[0] &= ~(0x02);

            //Clear flags
            bWaterInnerLeakSmall = false;
            bWaterInnerLeakLarge = false;
            ucWaterInnerLeakCount = 0;

            //Clear Modbus register flags
            usMBInputReg[INPUTREG_WATER_LEVEL - 1] &= ~(0x000C);
        }

        if (bReceiveI2C(I2C0_BASE, ucMsgReceive, 3, ucAddressADC[0]))
        {
            if (!(ucMsgReceive[2] & 0x80)) //check for ST/DRDY = 0
            {
                if ((ucMsgReceive[0] & 0x80) != 0) //negative ADC value
                {
                    usMBInputReg[INPUTREG_WATER_INNER - 1] = 0;
                    usMBInputReg[INPUTREG_WATER_INNER_4 - 1] = 0;
                    usMBInputReg[INPUTREG_WATER_INNER_16 - 1] = 0;
                    usMBInputReg[INPUTREG_WATER_INNER_32 - 1] = 0;
                    usMBInputReg[INPUTREG_WATER_INNER_64 - 1] = 0;

                    //Clear counters and summation
                    usADCReadingNum[1] = 0;
                    ulADCReadingSum[1] = 0;
                }
                else //ADC value OK
                {
                    //Get ADC reading
                    usADCReadingTemp = (uint16_t) (ucMsgReceive[0] << 8);
                    usADCReadingTemp |= ucMsgReceive[1];

                    //Update counters and sum readings
                    usADCReadingNum[1]++;
                    ulADCReadingSum[1] += usADCReadingTemp;
                    usADCReadingNum4[1]++;
                    ulADCReadingSum4[1] += usADCReadingTemp;
                    usADCReadingNum16[1]++;
                    ulADCReadingSum16[1] += usADCReadingTemp;
                    usADCReadingNum32[1]++;
                    ulADCReadingSum32[1] += usADCReadingTemp;
                    usADCReadingNum64[1]++;
                    ulADCReadingSum64[1] += usADCReadingTemp;

                    //Calculate and store averages
                    if (bStopwatchADC2[0])
                    {
                        usMBInputReg[INPUTREG_WATER_INNER - 1] = (uint16_t) (ulADCReadingSum[1] /  usADCReadingNum[1]);
                        usADCReadingNum[1] = 0;
                        ulADCReadingSum[1] = 0;
                        bStopwatchADC2[0] = false;
                    }
                    else if (bStopwatchADC2[1]) //4s moving average
                    {
                        usMBInputReg[INPUTREG_WATER_INNER_4 - 1] = (uint16_t) (ulADCReadingSum4[1] /  usADCReadingNum4[1]);
                        usADCReadingNum4[1] = 0;
                        ulADCReadingSum4[1] = 0;
                        bStopwatchADC2[1] = false;

                        //Inner tank water level check
                        if (usMBInputReg[INPUTREG_WATER_INNER_4 - 1] < (((float) (WATER_INNER_LOW * WATER_MAX_VOLUME)) / 100.0f))
                        {
                            //Water level critically low
                            bWaterInnerLow = true;
                        }
                        else bWaterInnerLow = false;

                        if (bWaterInnerLeakLarge || bWaterInnerLow) usMBInputReg[INPUTREG_WATER_LEVEL - 1] |= 0x0008;
                        else usMBInputReg[INPUTREG_WATER_LEVEL - 1] &= ~(0x0008);
                    }
                    else if (bStopwatchADC2[2]) //16s moving average
                    {
                        usMBInputReg[INPUTREG_WATER_INNER_16 - 1] = (uint16_t) (ulADCReadingSum16[1] /  usADCReadingNum16[1]);
                        usADCReadingNum16[1] = 0;
                        ulADCReadingSum16[1] = 0;
                        bStopwatchADC2[2] = false;
                    }
                    else if (bStopwatchADC2[3]) //32s moving average
                    {
                        usMBInputReg[INPUTREG_WATER_INNER_32 - 1] = (uint16_t) (ulADCReadingSum32[1] /  usADCReadingNum32[1]);
                        usADCReadingNum32[1] = 0;
                        ulADCReadingSum32[1] = 0;
                        bStopwatchADC2[3] = false;
                    }
                    else if (bStopwatchADC2[4]) //64s moving average
                    {
                        usMBInputReg[INPUTREG_WATER_INNER_64 - 1] = (uint16_t) (ulADCReadingSum64[1] /  usADCReadingNum64[1]);
                        usADCReadingNum64[1] = 0;
                        ulADCReadingSum64[1] = 0;
                        bStopwatchADC2[4] = false;

                        //Reset stopwatch timer and counter
                        TimerLoadSet(WTIMER1_BASE, TIMER_A, SysCtlClockGet());
                        TimerEnable(WTIMER1_BASE, TIMER_A);

                        /*--------------------------------------- Inner Tank Leak Check ----------------------------------------*/

                        if (bWaterHistoryReady) //at least two readings needed
                        {
                            //Check if water levels are falling
                            if (usMBInputReg[INPUTREG_WATER_INNER_64 - 1] < usWaterInnerPrevious)
                            {
                                if (((usWaterInnerPrevious - usMBInputReg[INPUTREG_WATER_INNER_64 - 1]) //check for large leaks
                                / usMBInputReg[INPUTREG_WATER_INNER_64 - 1]) >= (((float) (usWaterLeakLarge * WATER_MAX_VOLUME)) / 1000.0f))
                                {
                                    bWaterInnerLeakSmall = false;
                                    bWaterInnerLeakLarge = true;
                                    ucWaterInnerLeakCount = 0;
                                }
                                else if (((usWaterInnerPrevious - usMBInputReg[INPUTREG_WATER_INNER_64 - 1]) //check for small leaks
                                / usMBInputReg[INPUTREG_WATER_INNER_64 - 1]) >= (((float) (usWaterLeakSmall * WATER_MAX_VOLUME)) / 1000.0f))
                                {
                                    //Small leak detected - continue checking for 3 minutes
                                    ucWaterInnerLeakCount++;

                                    if (ucWaterInnerLeakCount == WATER_INNER_NUMCHECKS)
                                    {
                                        bWaterInnerLeakSmall = true;
                                        bWaterInnerLeakLarge = false;
                                        ucWaterInnerLeakCount = 0;
                                    }
                                }
                                else
                                {
                                    //Clear leak flags and counter
                                    bWaterInnerLeakSmall = false;
                                    bWaterInnerLeakLarge = false;
                                    ucWaterInnerLeakCount = 0;
                                }
                            }
                        }
                        else
                        {
                            //Clear leak flags and counter
                            bWaterInnerLeakSmall = false;
                            bWaterInnerLeakLarge = false;
                            ucWaterInnerLeakCount = 0;
                        }

                        //Update water level history
                        usWaterInnerPrevious = usMBInputReg[INPUTREG_WATER_INNER_64 - 1];
                        if (!bWaterHistoryReady) bWaterHistoryReady = true;

                        //Signal small water leaks via Modbus
                        if (bWaterInnerLeakSmall) usMBInputReg[INPUTREG_WATER_LEVEL - 1] |= 0x0004;
                        else usMBInputReg[INPUTREG_WATER_LEVEL - 1] &= ~(0x0004);

                        //Signal large water leaks via Modbus
                        if (bWaterInnerLeakLarge || bWaterInnerLow) usMBInputReg[INPUTREG_WATER_LEVEL - 1] |= 0x0008;
                        else usMBInputReg[INPUTREG_WATER_LEVEL - 1] &= ~(0x0008);
                    }
                }
            }

            //Clear I2C error
            usMBInputReg[INPUTREG_I2C_ERRORS - 1] &= ~(MASK_I2C_ERROR_ADC1);
        }
        else usMBInputReg[INPUTREG_I2C_ERRORS - 1] |= MASK_I2C_ERROR_ADC1; //store I2C error

        /*---------------------------- Cooling Fan and M2 Lock Inputs (Expander U7) ----------------------------*/

        if (bReceiveI2C(I2C0_BASE, ucMsgReceive, 1, ucAddressExpander[EXPANDER_INPUTS]))
        {
            ucMsgReceive[0] ^= (1UL << 3);

            //Store 3x cooling fan statuses, 1x M2 magnet lock status
            usMBInputReg[INPUTREG_OTHER - 1] = (uint16_t) ~(ucMsgReceive[0]);

            //Clear I2C error
            usMBInputReg[INPUTREG_I2C_ERRORS - 1] &= ~(MASK_I2C_ERROR_U7);
        }
        else usMBInputReg[INPUTREG_I2C_ERRORS - 1] |= MASK_I2C_ERROR_U7; //store I2C error

        /*-------------------------------- Output Signals and LEDs (Expander U8) -------------------------------*/

        //Emergency inner pump shut-off
        if (ucMBCoils[0] & 0x01) ucExpanderOutputs[0] = 0x01;
        else ucExpanderOutputs[0] = 0x00;

        //Outer tank valve control
        if (usMBInputReg[INPUTREG_WATER_OUTER - 1] >= (((float) (WATER_OUTER_VALVE_HIGH * WATER_MAX_VOLUME)) / 100.0f))
        {
            ucExpanderOutputs[0] &= ~(0x12); //Valve & LED OFF
        }
        else if (usMBInputReg[INPUTREG_WATER_OUTER - 1] <= (((float) (WATER_OUTER_VALVE_LOW * WATER_MAX_VOLUME)) / 100.0f))
        {
            ucExpanderOutputs[0] |= 0x12; //Valve & LED ON
        }

        //Small water leak
        if (bWaterInnerLeakSmall) ucExpanderOutputs[0] |= 0x04; //alert ON
        else ucExpanderOutputs[0] &= ~(0x04); //alert OFF

        //Large water leak
        if (bWaterInnerLeakLarge || bWaterInnerLow) ucExpanderOutputs[0] |= 0x08; //alert ON
        else ucExpanderOutputs[0] &= ~(0x08); //alert OFF

        //Outer tank water low LED
        if (bWaterOuterLow) ucExpanderOutputs[0] |= 0x20; //LED ON
        else ucExpanderOutputs[0] &= ~(0x20); //LED OFF

        //Outer tank water high LED
        if (bWaterOuterHigh) ucExpanderOutputs[0] |= 0x40; //LED ON
        else ucExpanderOutputs[0] &= ~(0x40); //LED OFF

        //Invert outputs
        ucExpanderOutputs[0] = ~ucExpanderOutputs[0];

        //Write to expander
        if (bWriteI2C(I2C0_BASE, ucExpanderOutputs, 1, ucAddressExpander[EXPANDER_OUTPUTS]))
        {
            //Clear I2C error
            usMBInputReg[INPUTREG_I2C_ERRORS - 1] &= ~(MASK_I2C_ERROR_U8);
        }
        else usMBInputReg[INPUTREG_I2C_ERRORS - 1] |= MASK_I2C_ERROR_U8; //store I2C error

        /*---------------------------------------- LEDs (Expander U9) ------------------------------------------*/

        //Get 3x cooling fan statuses and 1x M2 magnet lock status
        if (!(usMBInputReg[INPUTREG_I2C_ERRORS - 1] & MASK_I2C_ERROR_U7))
        {
            ucExpanderOutputs[0] = (uint8_t) usMBInputReg[INPUTREG_OTHER - 1];
        }
        else ucExpanderOutputs[0] = 0;

        //Pump activity
        if (!(usMBInputReg[INPUTREG_I2C_ERRORS - 1] & MASK_I2C_ERROR_U6))
        {
            if ((usMBInputReg[INPUTREG_PUMPS - 1] & 0x07) != 0) ucExpanderOutputs[0] |= 0x10; //inner pumps active
            else ucExpanderOutputs[0] &= ~(0x10); //inner pumps inactive

            if ((usMBInputReg[INPUTREG_PUMPS - 1] & 0x38) != 0)
            {
                ucExpanderOutputs[0] |= 0x20; //outer pumps active
            }
            else ucExpanderOutputs[0] &= ~(0x20);
        }
        else ucExpanderOutputs[0] &= ~(0x30);

        //Small water leak
        if (bWaterInnerLeakSmall) ucExpanderOutputs[0] |= 0x40; //LED ON
        else ucExpanderOutputs[0] &= ~(0x40); //LED OFF

        //Large water leak
        if (bWaterInnerLeakLarge || bWaterInnerLow) ucExpanderOutputs[0] |= 0x80; //LED ON
        else ucExpanderOutputs[0] &= ~(0x80); //LED OFF

        //Invert outputs
        ucExpanderOutputs[0] = ~ucExpanderOutputs[0];

        //Write to expander
        if (bWriteI2C(I2C0_BASE, ucExpanderOutputs, 1, ucAddressExpander[EXPANDER_LED]))
        {
            //Clear I2C error
            usMBInputReg[INPUTREG_I2C_ERRORS - 1] &= ~(MASK_I2C_ERROR_U9);
        }
        else usMBInputReg[INPUTREG_I2C_ERRORS - 1] |= MASK_I2C_ERROR_U9; //store I2C error
    }
}

eMBErrorCode eMBRegHoldingCB(uint8_t *pucRegBuffer, uint16_t usAddress, uint16_t usNRegs, eMBRegisterMode eMode)
{
    eMBErrorCode eStatus = MB_ENOERR;
    uint16_t usRegIndex;
    uint8_t ucConvertBCD[6] = { 0 };

    usAddress -= 1;

    if ((usAddress >= HOLDING_REGISTERS_START) && (usAddress + usNRegs <= HOLDING_REGISTERS_START + HOLDING_REGISTERS_NUM))
    {
        usRegIndex = (uint16_t) (usAddress - HOLDING_REGISTERS_START);
        switch (eMode)
        {
            case MB_REG_READ:

                while (usNRegs > 0)
                {
                    //Read settings from protocol stack
                    *pucRegBuffer++ = (uint8_t) (usMBHoldingReg[usRegIndex] >> 8);
                    *pucRegBuffer++ = (uint8_t) (usMBHoldingReg[usRegIndex] & 0xFF);
                    usRegIndex++;
                    usNRegs--;
                }
                break;

            case MB_REG_WRITE:

                while (usNRegs > 0)
                {
                    //Store new setting in protocol stack
                    usMBHoldingReg[usRegIndex] = *pucRegBuffer++ << 8;
                    usMBHoldingReg[usRegIndex] |= *pucRegBuffer++;

                    //Construct BCD string
                    ucConvertBCD[0] = ((usMBHoldingReg[usRegIndex] >> 12) & 0xF) + '0';
                    ucConvertBCD[1] = ((usMBHoldingReg[usRegIndex] >> 8) & 0xF) + '0';
                    ucConvertBCD[2] = '.';
                    ucConvertBCD[3] = ((usMBHoldingReg[usRegIndex] >> 4) & 0xF) + '0';
                    ucConvertBCD[4] = (usMBHoldingReg[usRegIndex] & 0xF) + '0';
                    ucConvertBCD[5] = '\n';

                    //Convert BCD to floating point
                    if (usRegIndex == 0) usWaterLeakSmall = atof((const char*) ucConvertBCD); //small leak setting
                    else if (usRegIndex == 1) usWaterLeakLarge = atof((const char*) ucConvertBCD); //large leak setting

                    usRegIndex++;
                    usNRegs--;
                }

                break;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

void IntHandlerTimerADS1110_1(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(WTIMER0_BASE, true);
    TimerIntClear(WTIMER0_BASE, ui32status);

    //Update stopwatch
    ucStopwatchADS1110[0]++;

    //1s average flag
    bStopwatchADC1[0] = true;

    //Other average flags
    if (!(ucStopwatchADS1110[0] % 4)) bStopwatchADC1[1] = true;
    if (!(ucStopwatchADS1110[0] % 16)) bStopwatchADC1[2] = true;
    if (!(ucStopwatchADS1110[0] % 32)) bStopwatchADC1[3] = true;

    //64s flag and counter
    if (!(ucStopwatchADS1110[0] % 64))
    {
        bStopwatchADC1[4] = true;
        ucStopwatchADS1110[0] = 0;
    }
}

void IntHandlerTimerADS1110_2(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(WTIMER1_BASE, true);
    TimerIntClear(WTIMER1_BASE, ui32status);
    ucStopwatchADS1110[1]++;

    //1s flag
    bStopwatchADC2[0] = true;

    //4s, 16s, and 32s flags
    if (!(ucStopwatchADS1110[1] % 4)) bStopwatchADC2[1] = true;
    if (!(ucStopwatchADS1110[1] % 16)) bStopwatchADC2[2] = true;
    if (!(ucStopwatchADS1110[1] % 32)) bStopwatchADC2[3] = true;

    //64s flag and counter
    if (!(ucStopwatchADS1110[1] % 64))
    {
        bStopwatchADC2[4] = true;
        ucStopwatchADS1110[1] = 0;
    }
}

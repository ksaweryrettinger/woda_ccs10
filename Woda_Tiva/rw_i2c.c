#include "parameters.h"
#include "prototypes.h"

volatile bool bI2CTimeout = false;

bool bWriteI2C(uint32_t ulI2CBase, uint8_t *ucData, uint8_t ucByteNum, uint8_t ucAddress)
{
    //Error variable created for debugging purposes
    uint32_t ulErrorI2C = I2C_MASTER_ERR_NONE;
    uint8_t i;

    //Wait until line is ready
    xI2CMasterWait(ulI2CBase, &ulErrorI2C);

    //I2C send mode
    I2CMasterSlaveAddrSet(ulI2CBase, ucAddress, false);

    if (ucByteNum == 1) //single byte
    {
        //Send data byte
        I2CMasterDataPut(ulI2CBase, ucData[0]);
        I2CMasterControl(ulI2CBase, I2C_MASTER_CMD_SINGLE_SEND);
        xI2CMasterWait(ulI2CBase, &ulErrorI2C);
    }
    else //multiple bytes
    {
        for (i = 0; i < ucByteNum; i++)
        {
            I2CMasterDataPut(ulI2CBase, ucData[i]);

            if (i == 0) //first byte
            {
                I2CMasterControl(ulI2CBase, I2C_MASTER_CMD_BURST_SEND_START);
            }
            else if (i < (ucByteNum - 1)) //intermediate byte
            {
                I2CMasterControl(ulI2CBase, I2C_MASTER_CMD_BURST_SEND_CONT);
            }
            else //last byte
            {
                I2CMasterControl(ulI2CBase, I2C_MASTER_CMD_BURST_SEND_FINISH);
            }

            //Wait for acknowledgement
            xI2CMasterWait(ulI2CBase, &ulErrorI2C);

            //Terminate transaction in the event of errors
            if (ulErrorI2C != I2C_MASTER_ERR_NONE)
            {
                I2CMasterControl(ulI2CBase, I2C_MASTER_CMD_BURST_SEND_ERROR_STOP);
                break;
            }
        }
    }

    if (ulErrorI2C == I2C_MASTER_ERR_NONE) return true;
    else return false;
}

bool bReceiveI2C(uint32_t ulI2CBase, uint8_t *ucData, uint8_t ucByteNum, uint8_t ucAddress)
{
    //Error variable created for debugging purposes
    uint32_t ulErrorI2C = I2C_MASTER_ERR_NONE;
    uint8_t i;

    //Add delay to ensure previous errors are cleared
    SysCtlDelay(2500/3); //delay by 5 I2C clock cycles

    //Wait until line is ready
    xI2CMasterWait(ulI2CBase, &ulErrorI2C);

    //I2C receive mode
    I2CMasterSlaveAddrSet(ulI2CBase, ucAddress, true);

    if (ucByteNum == 1) //single byte
    {
        //Receive data byte
        I2CMasterControl(ulI2CBase, I2C_MASTER_CMD_SINGLE_RECEIVE);
        xI2CMasterWait(ulI2CBase, &ulErrorI2C);
        if (ulErrorI2C == I2C_MASTER_ERR_NONE) ucData[0] = (uint8_t) I2CMasterDataGet(ulI2CBase);
    }
    else //multiple bytes
    {
        for (i = 0; i < ucByteNum; i++)
        {
            if (i == 0) //first byte
            {
                I2CMasterControl(ulI2CBase, I2C_MASTER_CMD_BURST_RECEIVE_START);
            }
            else if (i < (ucByteNum - 1)) //intermediate byte
            {
                I2CMasterControl(ulI2CBase, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
            }
            else //last byte
            {
                I2CMasterControl(ulI2CBase, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
            }

            //Wait for acknowledgement
            xI2CMasterWait(ulI2CBase, &ulErrorI2C);

            //Read next data byte
            if (ulErrorI2C == I2C_MASTER_ERR_NONE)
            {
                ucData[i] = (uint8_t) I2CMasterDataGet(ulI2CBase);
            }
            else //terminate transaction in the event of errors
            {
                I2CMasterControl(ulI2CBase, I2C_MASTER_CMD_BURST_RECEIVE_ERROR_STOP);
                break;
            }
        }
    }

    if (ulErrorI2C == I2C_MASTER_ERR_NONE) return true;
    else return false;
}

void xI2CMasterWait(uint32_t ulI2CBase, uint32_t *pulErrorI2C)
{
    //Start I2C timeout timer
    bI2CTimeout = false;
    TimerLoadSet(TIMER1_BASE, TIMER_BOTH, I2C_MASTER_TIMEOUT);
    TimerEnable(TIMER1_BASE, TIMER_BOTH);

    //Wait for response
    while (I2CMasterBusy(ulI2CBase))
    {
        if (bI2CTimeout) //check for timeout
        {
            //Timeout occured
            *pulErrorI2C = I2C_MASTER_ERR_CLK_TOUT;
            return;
        }
    }

    TimerDisable(TIMER1_BASE, TIMER_BOTH); //response received, switch off timer

    //Check for other errors
    *pulErrorI2C = I2CMasterErr(ulI2CBase);
}

void IntHandlerTimerI2C(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER1_BASE, true);
    TimerIntClear(TIMER1_BASE, ui32status);
    bI2CTimeout = true;
}

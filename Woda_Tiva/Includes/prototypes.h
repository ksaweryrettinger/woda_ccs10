#ifndef INCLUDES_PROTOTYPES_H_
#define INCLUDES_PROTOTYPES_H_

#include "parameters.h"

/* ----------------------- I2C Function Prototypes ------------------------------------*/

bool bReceiveI2C(uint32_t, uint8_t*, uint8_t, uint8_t);
bool bWriteI2C(uint32_t, uint8_t*, uint8_t, uint8_t);
void xI2CMasterWait(uint32_t, uint32_t*);

/* -------------------- 1-Wire Master Function Prototypes -----------------------------*/

bool bWriteCommand1WM(uint8_t, uint8_t, uint8_t*, eTempMode*);
bool bReadStatus1WM(uint8_t, uint8_t*, eTempMode*);
bool bReadData1WM(uint8_t, uint8_t*, eTempMode*);

#endif

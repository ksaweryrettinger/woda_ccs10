#ifndef INCLUDES_PARAMETERS_ORIGINAL_H_
#define INCLUDES_PARAMETERS_ORIGINAL_H_

#include <limits.h>
#include "tm4c123gh6pm.h"

typedef enum eTempMode { DS2482100_Reset,
                         DS2482100_Configure,
                         DS18B20_StartConfigure,
                         DS18B20_Configure,
                         DS18B20_Reset,
                         DS18B20_StartConversion,
                         DS18B20_SensorBusy,
                         DS18B20_ConversionReady,
                         DS18B20_ReadTemperature,
                         Timeout,
                         ErrorSD,
                         ErrorI2C,
                         ConnectedI2C } eTempMode;

/* --------------------------- Application  -----------------------------------*/

#define NUM_TEMP_SENSORS    (4)
#define NUM_EXPANDERS       (4)
#define NUM_TEMP_BYTES      (2)

/* ---------------------------- Expanders  ------------------------------------*/

#define EXPANDER_PUMPS          (0)  //Expander U6
#define EXPANDER_INPUTS         (1)  //Expander U7
#define EXPANDER_OUTPUTS        (2)  //Expander U8
#define EXPANDER_LED            (3)  //Expander U9

/* ------------------------- Water Level (ADC)  -------------------------------*/

#define NUM_ADC                 (2)
#define ADC_SPS                 (15)
#define WATER_MAX_VOLUME        (0x7FFF)
#define WATER_INNER_NUMCHECKS   (3)     //# of minute samples for small leak checks
#define WATER_OUTER_VALVE_LOW   (75)    //valve low shut-off (% of max tank volume)
#define WATER_OUTER_VALVE_HIGH  (80)    //valve high shut-off (% of max tank volume)
#define WATER_OUTER_HIGH        (90)    //outer tank level overflow (% of max tank volume)
#define WATER_OUTER_LOW         (50)    //outer tank level too low (% of max tank volume)
#define WATER_INNER_LOW         (50)    //inner tank level too low (% of max tank volume)
#define WATER_LEAK_LARGE_DEF    (5.0f)  //default setting for large leak detection (in %.)
#define WATER_LEAK_SMALL_DEF    (1.0f)  //default setting for small leak detection (in %.)

/* ------------------------- DS18B20 Timeout ----------------------------------*/

#define DS18B20_TIMEOUT     (2) //2s second timeout

/* --------------------------- I2C Configuration  -----------------------------*/

#define I2C_SPEED           (100000) //100kHz
#define I2C_MASTER_TIMEOUT  (15000)  //equivalent to 30 I2C clock cycles
#define I2C_MSG_NUMBYTES    (2)

/* ------------------ 1-Wire Master Status Register Masks  --------------------*/

#define MASK_SD     (0x04)
#define MASK_1WB    (0x01)
#define MASK_RST    (0x10)
#define MASK_PPD    (0x02)
#define MASK_SBR    (0x20)

/* ------------------------ DS2482-100 (1WM) Commands  ------------------------*/

#define WCFG        (0xD2)  //write configuration
#define WCFG_CMD    (0xE1)  //configuration byte
#define DRST        (0xF0)  //device reset (DS2482)
#define WRST        (0xB4)  //1-wire reset
#define WRBYTE      (0xA5)  //write single byte
#define RDBYTE      (0x96)  //read single byte
#define WBIT        (0x87)  //write single bit
#define RDTS        (0x80)  //read time slot
#define SETRP       (0xE1)  //set read pointer
#define SETRP_SR    (0xF0)  //set read pointer at status register
#define SETRP_RD    (0xE1)  //set read pointer at read data register

/* ------------------------ DS18B20 (Sensor) Commands  ------------------------*/

#define SKPROM      (0xCC)
#define WRSCR       (0x4E)
#define RDSCR       (0xBE)
#define CONVT       (0x44)
#define REG_TL      (0x8A)  //TL = -10C
#define REG_TH      (0x64)  //TH = 100C
#define REG_CFG     (0x5F)  //11-bit resolution (R1 = 1, R0 = 0), 375ms conversion time

/* --------------------------- Modbus Configuration ---------------------------*/

#define MB_STELLARIS_DEBUG      (0)
#define MB_MODE                 (MB_RTU)
#define MB_SLAVEID              (0x01)
#define MB_PORT                 (0)
#define MB_BAUDRATE             (19200)
#define MB_PARITY               (MB_PAR_EVEN)

/* --------------------------- Modbus LED -------------------------------------*/

#define MB_LED_PERIPHERAL       (SYSCTL_PERIPH_GPIOB)
#define MB_LED_PORT             (GPIO_PORTB_BASE)
#define MB_LED_PIN              (GPIO_PIN_7)

/* --------------------------- Modbus UART ------------------------------------*/

#if (MB_STELLARIS_DEBUG)

   #define MB_UART_MODULE       (UART0_BASE)
   #define MB_UART_INT          (INT_UART0)
   #define MB_UART_PERIPH       (SYSCTL_PERIPH_UART0)
   #define MB_UART_GPIO_PORT    (GPIO_PORTA_BASE)
   #define MB_UART_GPIO_PIN_RX  (GPIO_PIN_0)
   #define MB_UART_GPIO_PIN_TX  (GPIO_PIN_1)
   #define MB_UART_GPIO_RX      (GPIO_PA0_U0RX)
   #define MB_UART_GPIO_TX      (GPIO_PA1_U0TX)

#else

   #define MB_UART_MODULE       (UART3_BASE)
   #define MB_UART_INT          (INT_UART3)
   #define MB_UART_PERIPH       (SYSCTL_PERIPH_UART3)
   #define MB_UART_GPIO_PORT    (GPIO_PORTC_BASE)
   #define MB_UART_GPIO_PIN_RX  (GPIO_PIN_6)
   #define MB_UART_GPIO_PIN_TX  (GPIO_PIN_7)
   #define MB_UART_GPIO_RX      (GPIO_PC6_U3RX)
   #define MB_UART_GPIO_TX      (GPIO_PC7_U3TX)

#endif

/* ------------------------- Modbus Registers ---------------------------------*/

#define COILS_START               (1)      //coils starting address
#define COILS_NUM                 (2)      //number of coils

#define DISCRETES_START           (10001)  //discretes starting addres
#define DISCRETES_NUM             (0)      //number of discrete inputs

#define INPUT_REGISTERS_START     (30001)  //input registers starting address
#define INPUT_REGISTERS_NUM       (20)     //number of input registers

#define HOLDING_REGISTERS_START   (40001)  //holding registers starting address
#define HOLDING_REGISTERS_NUM     (2)      //number of holding registers

#define REG_NUM_BYTES             (2)      //number of bytes in register
#define REG_ADDR_OFFSET           (1)      //bit/register address offset

/* ------------------- Modbus Register Configuration --------------------------*/

//INPUT REGISTERS - TEMPERATURE READINGS
#define INPUTREG_TEMP_START       (1)
#define INPUTREG_TEMP_END         (4)
#define INPUTREG_ERROR_SD         (5)
#define INPUTREG_ERROR_TIME       (6)

//INPUT REGISTER - I2C ERRORS
#define INPUTREG_I2C_ERRORS       (7)

//I2C ERROR MASKS
#define MASK_I2C_ERROR_ADC1  (0x0010)
#define MASK_I2C_ERROR_ADC2  (0x0020)
#define MASK_I2C_ERROR_U6    (0x0040)
#define MASK_I2C_ERROR_U7    (0x0080)
#define MASK_I2C_ERROR_U8    (0x0100)
#define MASK_I2C_ERROR_U9    (0x0200)

//INPUT REGISTERS - EXPANDERS
#define INPUTREG_PUMPS            (8)
#define INPUTREG_OTHER            (9)

//INPUT REGISTERS - WATER LEVELS (INNER TANK)
#define INPUTREG_WATER_INNER      (10)
#define INPUTREG_WATER_INNER_4    (11)
#define INPUTREG_WATER_INNER_16   (12)
#define INPUTREG_WATER_INNER_32   (13)
#define INPUTREG_WATER_INNER_64   (14)

//INPUT REGISTERS - WATER LEVELS (OUTER TANK)
#define INPUTREG_WATER_OUTER      (15)
#define INPUTREG_WATER_OUTER_4    (16)
#define INPUTREG_WATER_OUTER_16   (17)
#define INPUTREG_WATER_OUTER_32   (18)
#define INPUTREG_WATER_OUTER_64   (19)

//INPUT REGISTERS - WATER LEVEL ALERTS
#define INPUTREG_WATER_LEVEL      (20)

/* --------------------- Modbus Callback Functions ----------------------------*/

#define MBCB_COILS_DEFAULT      (1)
#define MBCB_DISCRETE_DEFAULT   (1)
#define MBCB_INPUTREG_DEFAULT   (1)
#define MBCB_HOLDING_DEFAULT    (0)

/* ------------------------- Modbus Global Registers --------------------------*/

uint8_t  ucMBCoils[(COILS_NUM + (CHAR_BIT - 1)) / CHAR_BIT];            //Coils
uint8_t  ucMBDiscretes[(DISCRETES_NUM + (CHAR_BIT - 1)) / CHAR_BIT];    //Discrete Inputs
uint16_t usMBInputReg[INPUT_REGISTERS_NUM];                             //Input Registers
uint16_t usMBHoldingReg[HOLDING_REGISTERS_NUM];                         //Holding Registers

/* ------------------------- Macro Used to Hide Warnings ----------------------*/

#define UNREFERENCED(x)  ((void)x)

/*------------------------------ Modbus Data ----------------------------------*/

/*
 * Input Registers 1-4 = Temperature (DS18B20 Registers)
 * Input Register 5 = Expander Inputs (Bits 0-7)
 * Input Register 6 = I2C Errors (Bits 0-3)
 * Input Register 7 = SD Errors (Bits 0-3)
 * Input Register 8 = Timeout Errors (Bits 0-3)
 *
 */

/*------------------------ GPIO Pin Configuration -----------------------------*/

/*
 * PA6 - I2C-1 (SCL)
 * PA7 - I2C-1 (SDA)
 *
 * PB2 - I2C-2 (SCL)
 * PB3 - I2C-2 (SDA)
 *
 * PB7 - GPIO Output (Modbus LED)
 * PC6 - UART Rx (Modbus)
 * PC7 - UART Tx (Modbus)
 *
 * PD1 - GPIO Output
 * PD2 - GPIO Output
 * PD3 - GPIO Output
 *
 */

#endif //INCLUDES_PARAMETERS_H_

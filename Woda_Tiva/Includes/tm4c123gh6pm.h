/* TivaWare and Standard C Library Includes */

#ifndef INCLUDES_TM4C123GH6PM_H_
#define INCLUDES_TM4C123GH6PM_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_i2c.h"
#include "driverlib/timer.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/gpio.h"
#include "driverlib/uart.h"
#include "driverlib/i2c.h"
#include "driverlib/fpu.h"

#endif /* INCLUDES_TM4C123GH6PM_H_ */
